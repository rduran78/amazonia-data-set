# Amazon Region Socioeconomic Grid Dataset Description

## File Information
**Shapefile (SHP):** `base_5x5_adjusted.shp`  
**CSV Filename:** `baseCSV_BID.csv`

## Description
This dataset includes a 5x5 km gridded socioeconomic profile of the Amazon region, as delineated by RAISG (Amazon Network of Georeferenced Socio-Environmental Information). It covers key variables such as Gross Domestic Product (GDP) and population estimates. The dataset facilitates detailed spatial analysis of economic and demographic variables.

## Data Sources and Methodology
- **GDP Data:** Derived from the study by Chen et al. (2022), titled "Global 1 km×1 km gridded revised real gross domestic product and electricity consumption during 1992–2019 based on calibrated nighttime light data." Adjustments were made using a random forest model to align with official statistics from Brazil, Bolivia, and Ecuador.
- **Population Data:** Based on Freire (2016) "Development of new open and free multi-temporal global population grids at 250 m resolution," rescaled to the 5x5 km grid.
- **Additional Data:** The dataset includes deforestation data and GDP adjustments, enhancing its utility for diverse analytical purposes.

## Variable Description
| Indicator   | Name                       | Unit                         | Description                                                  | Source                                                   |
|-------------|----------------------------|------------------------------|--------------------------------------------------------------|----------------------------------------------------------|
| GDP         | Gross Domestic Product     | Millions of USD (2017 base)  | Estimated GDP based on night-time light data                 | Chen, J., et al. (2022)                                  |
| DEF         | Deforestation              | Deforestation pixels (900 m²)| Deforestation data sourced from RAISIG                       | RAISG. (2023)                                            |
| POP         | Population                 | Individual                    | Population data sourced from Global Human Settlement         | GHLS. (2023)                                             |
| GDP_AD      | Adjusted GDP               | Millions of USD (2017 base)  | GDP adjusted using a random forest model                     | Self-produced using data from Chen, J., et al. (2022)    |
| DEF_A       | Accumulated Deforestation  | Deforestation pixels (900 m²)| Cumulative deforestation from 2001 to 2021                   | Self-produced using data from RAISG. (2023)              |
| ID_ESTADO   | State Identifier           | -                            | Unique identifier for states within the dataset              |                                                          |
| ID_MUN      | Municipality Identifier    | -                            | Unique identifier for municipalities within the dataset      |                                                          |
| ID_PE       | Country and State Identifier | -                          | Combined identifier for country and state observations       |                                                          |
| ID_PEM      | Country, State, and Municipality Identifier | -          | Combined identifier for specific country, state, and municipality observations |                              |

## Recommended Citation
Figueroa, D., & Duran-Fernandez, R. (2024). Amazon Region Socioeconomic Grid Dataset. Retrieved from [Amazonia Data Set Repository](https://gitlab.com/rduran78/amazonia-data-set).

